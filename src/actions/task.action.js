import {TASK_INPUT_CHANGE_HANDLER, TASK_FILTER_BUTTON_HANDLER} from "../constants/task.constants"

//mô tả những sự kiện liên quan tới task (chứa các hàm return obj)
const inputChangeHandler =(value)=>{
    return {
        type: TASK_INPUT_CHANGE_HANDLER,
        payload: value
    }
}  
const buttonFilterHandler = () =>{
    return {
        type: TASK_FILTER_BUTTON_HANDLER
    }
}

export {
    inputChangeHandler,
    buttonFilterHandler,
}